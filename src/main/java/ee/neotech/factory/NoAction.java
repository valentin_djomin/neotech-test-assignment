package ee.neotech.factory;

public class NoAction extends AbstractAction {

    @Override
    public void execute() {
        System.out.println("Wrong arguments! Program should be executed without arguments or with parameter -p.");
    }
}
