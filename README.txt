The aim of this command line application is
 to write timestamps to MySQL database and read all timestamps stored in database in ascending order.

To run application on the local environment:
1) Run MySQL database with docker-compose(exec "docker-compose up -d" from /neotech-test-assignment/docker).
It will create initial database schema and will run 2 containers: mysql-development and adminer.
mysql-development is for database. Exposed port: 3308
adminer is UI inis a full-featured database management tool for browser. Exposed port:8080
2) Run application without arguments or with argument -p
2.1) through the IDE with Main class
2.2) Build fat jar with "gradle build" command and run application as jar "java -jar neotech-test-assignment-1.0-SNAPSHOT.jar"
