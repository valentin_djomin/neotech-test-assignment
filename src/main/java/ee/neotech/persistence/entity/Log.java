package ee.neotech.persistence.entity;

import java.sql.Timestamp;
import java.util.Objects;

public class Log {
    private Integer id;
    private Timestamp timestamp;

    public Log(Integer id, Timestamp timestamp) {
        this.id = id;
        this.timestamp = timestamp;
    }

    public void print() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "ID: " + id + " | TIMESTAMP: " + timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Log log = (Log) o;
        return Objects.equals(id, log.id) &&
                Objects.equals(timestamp, log.timestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp);
    }
}
