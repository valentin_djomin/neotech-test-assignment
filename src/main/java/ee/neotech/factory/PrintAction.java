package ee.neotech.factory;

import ee.neotech.persistence.entity.Log;
import ee.neotech.service.LogService;

public class PrintAction extends AbstractAction {
    @Override
    public void execute() {
        LogService.getLogs().forEach(Log::print);
    }
}
