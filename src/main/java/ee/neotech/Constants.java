package ee.neotech;

public class Constants {

    public static final int TIMESTAMP_QUEUE_CAPACITY = 1024;
    public static final long TIMESTAMP_TASK_DELAY = 0L;
    public static final long TIMESTAMP_TASK_PERIOD = 1000L;
    public static final String MAIN_ARGUMENT_PRINT = "-p";

    public static final int RETRY_MAX_ATTEMPTS = 12;
    public static final long RETRY_INTERVAL_MILLIS = 5000L;
    public static final String RETRY_NAME_INSERT = "retryInsert";



}
