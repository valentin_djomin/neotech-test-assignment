package ee.neotech.util;

import ee.neotech.Constants;
import io.github.resilience4j.core.IntervalFunction;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;

import java.util.function.Function;

public class RetryUtil {

    public static <T, R> Function<T, R> getRetryableFunction(final String retryName, final Function<T, R> function) {
        final RetryRegistry registry =
                RetryRegistry.of(
                        RetryConfig.custom()
                                .maxAttempts(Constants.RETRY_MAX_ATTEMPTS)
                                .intervalFunction(IntervalFunction.of(Constants.RETRY_INTERVAL_MILLIS))
                                .retryExceptions(RuntimeException.class)
                                .build());
        final Retry insertRetry = registry.retry(retryName);
        return Retry.decorateFunction(insertRetry, function);
    }
}
