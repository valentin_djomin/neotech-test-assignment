package ee.neotech.tasks;

import ee.neotech.Constants;
import ee.neotech.persistence.MysqlConnecter;
import ee.neotech.service.LogService;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class InserterTaskTest {

    @Test
    public void inserterTask() throws Exception {
        final BlockingQueue<Timestamp> timestamps = new ArrayBlockingQueue<>(Constants.TIMESTAMP_QUEUE_CAPACITY);
        timestamps.add(Timestamp.from(Instant.now()));
        timestamps.add(Timestamp.from(Instant.now()));
        timestamps.add(Timestamp.from(Instant.now()));
        final InserterTask inserterTask = new InserterTask(timestamps);

        final int initialLogCount = LogService.getLogs().size();
        final ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(inserterTask);
        executorService.awaitTermination(3000L, TimeUnit.MILLISECONDS);

        assertEquals(initialLogCount + 3, LogService.getLogs().size());
    }
}
