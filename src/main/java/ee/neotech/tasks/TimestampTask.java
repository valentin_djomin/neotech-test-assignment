package ee.neotech.tasks;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;

public class TimestampTask extends TimerTask {

    private BlockingQueue<Timestamp> blockingQueue;

    public TimestampTask(BlockingQueue<Timestamp> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            blockingQueue.put(Timestamp.from(Instant.now()));
        } catch (Exception e) {
            System.out.println("Cannot add created timestamp to queue. Message: " + e.getMessage());
        }
    }


}
