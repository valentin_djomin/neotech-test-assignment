package ee.neotech.factory;

public abstract class AbstractAction {

    public abstract void execute();

}
