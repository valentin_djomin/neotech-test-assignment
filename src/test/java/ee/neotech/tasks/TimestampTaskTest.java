package ee.neotech.tasks;

import ee.neotech.Constants;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TimestampTaskTest {

    @Test
    public void timerTask_sync() {
        final BlockingQueue<Timestamp> testQueue = new ArrayBlockingQueue<>(Constants.TIMESTAMP_QUEUE_CAPACITY);
        final TimestampTask timestampTask = new TimestampTask(testQueue);
        timestampTask.run();
        timestampTask.run();
        timestampTask.run();

        assertEquals(3, testQueue.size());
    }

    @Test
    public void timerTask_async_3elementsInserted() throws Exception {
        final BlockingQueue<Timestamp> timestamps = new ArrayBlockingQueue<>(Constants.TIMESTAMP_QUEUE_CAPACITY);
        final TimestampTask timestampTask = new TimestampTask(timestamps);

        final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(timestampTask, 200, 1000L, TimeUnit.MILLISECONDS);
        scheduledExecutorService.awaitTermination(3000L, TimeUnit.MILLISECONDS);

        assertEquals(3, timestamps.size());
    }

    @Test
    public void timerTask_async_5elementsInserted() throws Exception {
        final BlockingQueue<Timestamp> timestamps = new ArrayBlockingQueue<>(Constants.TIMESTAMP_QUEUE_CAPACITY);
        final TimestampTask timestampTask = new TimestampTask(timestamps);

        final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        scheduledExecutorService.scheduleAtFixedRate(timestampTask, 200, 1000L, TimeUnit.MILLISECONDS);
        scheduledExecutorService.awaitTermination(5000L, TimeUnit.MILLISECONDS);

        assertEquals(5, timestamps.size());
    }

}
