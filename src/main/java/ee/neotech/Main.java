package ee.neotech;

import ee.neotech.factory.ActionFactory;
import ee.neotech.persistence.MysqlConnecter;

public class Main {

    public static void main(String[] args) {
        try {
            ActionFactory
                    .getAction(args)
                    .execute();
        } finally {
            MysqlConnecter.shutdown();
        }
    }
}
