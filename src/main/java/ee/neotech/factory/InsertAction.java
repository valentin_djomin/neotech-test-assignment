package ee.neotech.factory;

import ee.neotech.Constants;
import ee.neotech.tasks.InserterTask;
import ee.neotech.tasks.TimestampTask;

import java.sql.Timestamp;
import java.util.Timer;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class InsertAction extends AbstractAction {
    @Override
    public void execute() {
        final BlockingQueue<Timestamp> timestampQueue = new LinkedBlockingQueue<>();
        new Timer().schedule(new TimestampTask(timestampQueue), Constants.TIMESTAMP_TASK_DELAY, Constants.TIMESTAMP_TASK_PERIOD);
        new Thread(new InserterTask(timestampQueue)).start();
    }
}
