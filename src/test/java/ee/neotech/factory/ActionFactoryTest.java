package ee.neotech.factory;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ActionFactoryTest {

    @Test
    void getAction_shouldReturnInsertAction() {
        final String[] args = new String[]{};
        final AbstractAction action = ActionFactory.getAction(args);
        assertTrue(action instanceof InsertAction);
    }

    @Test
    void getAction_shouldReturnPrintAction() {
        final String[] args = new String[]{"-p"};
        final AbstractAction action = ActionFactory.getAction(args);
        assertTrue(action instanceof PrintAction);
    }

    @Test
    void getAction_shouldReturnNoAction() {
        final AbstractAction action = ActionFactory.getAction(new String[]{"-p", "-p"});
        assertTrue(action instanceof NoAction);

        final AbstractAction action2 = ActionFactory.getAction(new String[]{"-t"});
        assertTrue(action2 instanceof NoAction);
    }
}
