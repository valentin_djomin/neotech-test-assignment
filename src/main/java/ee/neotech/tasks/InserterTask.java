package ee.neotech.tasks;

import ee.neotech.Constants;
import ee.neotech.service.LogService;
import ee.neotech.util.RetryUtil;

import java.sql.Timestamp;
import java.util.concurrent.BlockingQueue;
import java.util.function.Function;

public class InserterTask implements Runnable {

    private BlockingQueue<Timestamp> blockingQueue;
    private Function<Timestamp, Void> retryableFunction;

    public InserterTask(BlockingQueue<Timestamp> blockingQueue) {
        this.blockingQueue = blockingQueue;
        this.retryableFunction = RetryUtil.getRetryableFunction(Constants.RETRY_NAME_INSERT,
                (Timestamp s) -> {
                    LogService.insert(s);
                    return null;
                });
    }

    @Override
    public void run() {
        while (true) {
            try {
                retryableFunction.apply(blockingQueue.take());
            } catch (Exception e) {
                System.out.println("Retrying is aborted due to max amount of attempts was exceeded. Max amount is " + Constants.RETRY_MAX_ATTEMPTS);
            }
        }
    }

}
