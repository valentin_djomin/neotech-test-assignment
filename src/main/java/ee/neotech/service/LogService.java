package ee.neotech.service;

import ee.neotech.persistence.MysqlConnecter;
import ee.neotech.persistence.entity.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class LogService {

    public static void insert(final Timestamp ts) throws RuntimeException {
        final String query = "INSERT INTO logs(date) VALUES (?)";
        try {
            final Connection connection = MysqlConnecter.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setTimestamp(1, ts);
            preparedStatement.execute();
            connection.commit();
            preparedStatement.close();
            System.out.println("Data inserted. TS: " + ts);
        } catch (Exception e) {
            MysqlConnecter.shutdown();
            throw new RuntimeException(e.getMessage());
        }
    }

    public static List<Log> getLogs() throws RuntimeException {
        final List<Log> logs = new ArrayList<>();
        final String query = "SELECT * FROM logs";
        try {
            final Connection connection = MysqlConnecter.getConnection();
            final Statement statement = connection.createStatement();
            final ResultSet resultSet = statement
                    .executeQuery(query);

            while (resultSet.next()) {
                final int id = resultSet.getInt("id");
                final Timestamp timestamp = resultSet.getTimestamp("date");
                logs.add(new Log(id, timestamp));
            }

            statement.close();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        } finally {
            MysqlConnecter.shutdown();
        }
        return logs;
    }
}
