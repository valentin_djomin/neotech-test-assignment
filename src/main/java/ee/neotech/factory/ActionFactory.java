package ee.neotech.factory;

import ee.neotech.Constants;

public class ActionFactory {

    public static AbstractAction getAction(final String[] args) {
        if (args.length == 0) {
            return new InsertAction();
        } else if (args.length == 1 && Constants.MAIN_ARGUMENT_PRINT.equals(args[0])) {
            return new PrintAction();
        } else {
            return new NoAction();
        }
    }

}
