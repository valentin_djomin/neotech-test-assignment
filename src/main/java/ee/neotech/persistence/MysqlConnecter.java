package ee.neotech.persistence;

import java.sql.Connection;
import java.sql.DriverManager;

public class MysqlConnecter {
    private static String MYSQL_DRIVER = "com.mysql.cj.jdbc.Driver";
    private static String MYSQL_URL = "jdbc:mysql://localhost:3308/timestamps";
    private static String MYSQL_ROOT_USER = "root";
    private static String MYSQL_ROOT_PASSWORD = "123";

    private static Connection connection;

    public static Connection getConnection() throws Exception {
        if (connection != null) {
            return connection;
        }

        Class.forName(MYSQL_DRIVER);
        connection = DriverManager.getConnection(MYSQL_URL, MYSQL_ROOT_USER, MYSQL_ROOT_PASSWORD);
        connection.setAutoCommit(Boolean.FALSE);
        System.out.println("Connection with DB established.");
        return connection;
    }


    public static void shutdown() {
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
        } catch (Exception e) {
        } finally {
            System.out.println("DB disconnected.");
        }
    }
}
